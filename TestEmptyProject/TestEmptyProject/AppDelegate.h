//
//  AppDelegate.h
//  TestEmptyProject
//
//  Created by HU Ziming on 13-2-25.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
