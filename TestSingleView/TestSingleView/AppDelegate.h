//
//  AppDelegate.h
//  TestSingleView
//
//  Created by HU Ziming on 13-1-13.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
