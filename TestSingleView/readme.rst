This is an single view project created by XCode, which is used for reference.

Here is the difference between single view project and empty project:

AppDelegate.h
=======================

In empty project, this file only contains ``@interface AppDelegate``,
while in single view project, there is a new class declaration ``@class ViewController;``,
and a new *@property* line ``@property (strong, nonatomic) ViewController *viewController;``

AppDelegate.m
=======================

The only difference is occurred in function ``didFinishLaunchingWithOptions``.
In single view project, the property ``self.viewController`` is initialized with a *nib* file (which is also created by default in single view project),
and the ``viewController`` will be set to *rootViewController* of the window.
While in empty project, the program will only set the background color of window by this ``self.window.backgroundColor = [UIColor whiteColor];``


The *main.m* file and *<ProjectName>-Info.plist* file are functionally same.



