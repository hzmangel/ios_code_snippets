//
//  AppDelegate.h
//  SimpleTableViewProgram
//
//  Created by HU Ziming on 13-1-13.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tab_bar;

@end
