//
//  SectionTableViewController.h
//  SimpleTableViewProgram
//
//  Created by HU Ziming on 13-2-27.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

- (void) generateDataSet;

@end
