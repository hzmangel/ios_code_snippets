//
//  SectionTableViewController.m
//  SimpleTableViewProgram
//
//  Created by HU Ziming on 13-2-27.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import "SectionTableViewController.h"

@interface SectionTableViewController ()

@end

@implementation SectionTableViewController {
    NSArray *table_data;
    NSArray *table_idx;
    NSMutableDictionary *table_dict;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"section";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self generateDataSet];
}

// Necessary function needed for section table view list
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return table_idx.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Number of rows it should expect should be based on the section
    return [[table_dict objectForKey:[table_idx objectAtIndex:section]] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [table_idx objectAtIndex:section];
}

// Necessary functions for simple table view list
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }

    cell.textLabel.text = [[table_dict objectForKey:[table_idx objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];

    return cell;
}

// Indexing the table view
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return table_idx;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return index;
}

// Response to the row click event
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *alert_title = [[table_dict objectForKey:[table_idx objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    NSString *alert_msg = [NSString stringWithFormat:@"section %d\nrow %d", indexPath.section, indexPath.row];
    UIAlertView *message_alert = [[UIAlertView alloc] initWithTitle:alert_title message:alert_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [message_alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Function for generating data
- (void) generateDataSet {
    // Initalization
    NSMutableArray *tmp_idx = [[NSMutableArray alloc] init];
    NSMutableDictionary *tmp_grouped_cont = [[NSMutableDictionary alloc] init];
    NSPredicate *filter_predicate;


    // Country list
    NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"country_list" ofType:@"txt"] encoding:NSMacOSRomanStringEncoding error:nil];
    table_data = [fileText componentsSeparatedByString:@"\n"];


    // Category for country list
    for (NSString *country_name in table_data) {
        if ((country_name.length == 0) || ([tmp_idx count] != 0 && [tmp_idx containsObject:[country_name substringToIndex:1]]))
            continue;

        [tmp_idx addObject:[country_name substringToIndex:1]];
    }
    table_idx = tmp_idx;


    // Generate sectioned diectionary
    for (NSString *idx in table_idx) {
        filter_predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", idx];
        [tmp_grouped_cont setValue:[table_data filteredArrayUsingPredicate:filter_predicate] forKey:idx];
    }
    table_dict = tmp_grouped_cont;
}

@end
