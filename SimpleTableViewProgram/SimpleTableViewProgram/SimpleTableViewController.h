//
//  SimpleTableViewController.h
//  SimpleTableViewProgram
//
//  Created by HU Ziming on 13-2-26.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *table_view;
@property (strong, nonatomic) IBOutlet UISearchBar *search_bar;

- (void) generateDataSet;
- (void) searchTableView;

@end
