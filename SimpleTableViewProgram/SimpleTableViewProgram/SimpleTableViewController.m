//
//  SimpleTableViewController.m
//  SimpleTableViewProgram
//
//  Created by HU Ziming on 13-2-26.
//  Copyright (c) 2013年 HZM. All rights reserved.
//

#import "SimpleTableViewController.h"

@interface SimpleTableViewController ()

@end

@implementation SimpleTableViewController {
    NSArray *table_data;
    NSArray *table_idx;
    
    BOOL is_searching;
    BOOL can_select_row;
    NSArray *search_rslt;
}

@synthesize search_bar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"simple";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    search_bar.autocorrectionType = UITextAutocorrectionTypeNo;
    is_searching = NO;
    can_select_row = YES;
    search_rslt = [[NSMutableArray alloc] init];
    
    [self generateDataSet];
}

// Necessary function needed for simple table view list
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (is_searching) {
        return search_rslt.count;
    } else {
        return table_data.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }
    
    if (is_searching) {
        cell.textLabel.text = [search_rslt objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [table_data objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (NSIndexPath *)tableView :(UITableView *)theTableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (can_select_row) {
        return indexPath;
    } else {
        return nil;
    }
}

// Function for search
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    is_searching = YES;
    can_select_row = NO;
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 0) {
        is_searching = YES;
        can_select_row = YES;
        self.table_view.scrollEnabled = NO;
        [self searchTableView];
    } else {
        is_searching = NO;
        can_select_row = NO;
        self.table_view.scrollEnabled = YES;
    }
    
    [self.table_view reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self searchTableView];
    
    self.search_bar.text = @"";
    [self.search_bar resignFirstResponder];

    can_select_row = YES;
    is_searching = NO;
    self.table_view.scrollEnabled = YES;
    
    [self.table_view reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Function for generating data
- (void) generateDataSet {
    // Initalization
    NSMutableArray *tmp_idx = [[NSMutableArray alloc] init];


    // Country list
    NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"country_list" ofType:@"txt"] encoding:NSMacOSRomanStringEncoding error:nil];
    table_data = [fileText componentsSeparatedByString:@"\n"];


    // Category for country list
    for (NSString *country_name in table_data) {
        if ((country_name.length == 0) || ([tmp_idx count] != 0 && [tmp_idx containsObject:[country_name substringToIndex:1]]))
            continue;

        [tmp_idx addObject:[country_name substringToIndex:1]];
    }
    table_idx = tmp_idx;
}

// Function for searching
- (void) searchTableView {
    NSString *search_text = self.search_bar.text;
    NSPredicate *filter_predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", search_text];
    search_rslt = [table_data filteredArrayUsingPredicate:filter_predicate];
}

@end
